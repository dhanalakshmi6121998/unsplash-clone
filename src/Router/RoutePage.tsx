import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import HomePage from '../HomePage/HomePage';

const RoutePage = () => {
  return (
    <Router>
      <Routes>
        <Route path='/' element={<HomePage />} />
        <Route path='/wallpapers' element={<HomePage />} />
        <Route path='/travel' element={<HomePage />} />
        <Route path='/nature' element={<HomePage />} />
      </Routes>
    </Router>
  );
};

export default RoutePage;
