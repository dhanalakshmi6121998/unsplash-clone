import React from 'react';
import RoutePage from './Router/RoutePage';
import './App.css';

function App() {
  return (
    <div className='App'>
      <RoutePage />
    </div>
  );
}

export default App;
