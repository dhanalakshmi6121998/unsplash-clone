export interface ISearchInput {
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onClick: () => void;
  searchValue: string;
}
