export const getActivePage = (pathname: string) => {
  switch (pathname) {
    case '/wallpapers':
      return 'wallpapers';
    case '/travel':
      return 'travel';
    case '/nature':
      return 'nature';
    case '/':
      return 'homepage';
    default:
      return '';
  }
};
