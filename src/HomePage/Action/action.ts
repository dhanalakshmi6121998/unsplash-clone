import Types from './type';
import axios from 'axios';

const getUSer = (images: any[], page: string) => ({
  type: Types.GET_EDITORIAL_IMAGES,
  payload: {
    page,
    images,
  },
});

export const setLikedImageDetails = (pageName: string, imageId: string) => ({
  type: Types.SET_LIKE_IMAGE,
  payload: {
    pageName,
    imageId,
  },
});

export const setLikeImages = (pageName: string, imageId: string): any => {
  return function (dispatch: any) {
    dispatch(setLikedImageDetails(pageName, imageId));
  };
};

export const getPhotos = (page: string): any => {
  return function (dispatch: any) {
    axios
      .get(
        `https://api.unsplash.com/photos?client_id=${process.env.REACT_APP_CLIENT_ID}&per_page=20`
      )
      .then((res: any) => {
        dispatch(getUSer(res.data, page));
      })
      .catch((error: any) => console.log(error));
  };
};

export const getSearchList = (searchValue: string, page: string): any => {
  return function (dispatch: any) {
    axios
      .get(
        `https://api.unsplash.com/search/photos?client_id=${process.env.REACT_APP_CLIENT_ID}&query=${searchValue}&per_page=20`
      )
      .then((res: any) => {
        console.log('res.results', res);
        dispatch(getUSer(res.data.results, page));
      })
      .catch((error: any) => console.log(error));
  };
};
