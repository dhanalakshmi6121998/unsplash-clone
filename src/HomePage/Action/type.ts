const Types = {
  GET_EDITORIAL_IMAGES: 'GET_EDITORIAL_IMAGES',
  SET_LIKE_IMAGE: 'SET_LIKE_IMAGE',
};

export default Types;
