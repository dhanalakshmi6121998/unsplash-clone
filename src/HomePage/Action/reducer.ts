import Types from './type';

const initialState: any = {
  loading: false,
  editorialImages: {
    homepage: [],
    wallpapers: [],
    travel: [],
    nature: [],
  },
  likedImage: {},
  error: '',
};

const userReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case Types.GET_EDITORIAL_IMAGES:
      const { page, images } = action.payload || {};
      return {
        ...state,
        error: '',
        editorialImages: {
          ...state.editorialImages,
          [page]: images,
        },
      };
    case Types.SET_LIKE_IMAGE:
      const { pageName, imageId } = action.payload || {};
      const likedImageList = state.likedImage[pageName] || [];
      const isIncluded = state.likedImage[pageName]?.includes(imageId);
      let likedImages: string[] = [];
      if (isIncluded) {
        const removeLike = likedImageList.filter((imgId: string) => imgId !== imageId);
        likedImages = removeLike;
      } else {
        likedImages = [...likedImageList, imageId];
      }
      return {
        ...state,
        error: '',
        likedImage: {
          ...state.likedImage,
          [pageName]: likedImages,
        },
      };
    default:
      return state;
  }
};

export default userReducer;
