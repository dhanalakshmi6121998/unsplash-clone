import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import Header from '../Components/Header/Header';
import ImageCard from '../Components/ImageCard/ImageCard';
import Navbar from '../Components/Navbar/Navbar';
import { getPhotos, getSearchList, setLikeImages } from './Action/action';
import { getActivePage } from './fixture';
import styles from './HomePage.module.scss';

function HomePage(props: any) {
  const imageDetails = useSelector((store: any) => store.images);
  const location = useLocation();
  const dispatch = useDispatch();

  const { editorialImages = {}, likedImage = {} } = imageDetails || {};
  const [activePath, setActivePath] = useState('');
  const [images, setImages] = useState<any>([]);
  const [searchValue, setSearchValue] = useState('');

  useEffect(() => {
    const pathname = getActivePage(location.pathname);
    if (editorialImages[pathname]?.length === 0) {
      if (pathname === 'homepage') dispatch(getPhotos(pathname));
      else dispatch(getSearchList(pathname, pathname));
    }
    setActivePath(pathname);
  }, [location.pathname]);

  useEffect(() => {
    if (activePath && Object.keys(editorialImages || {}).length !== 0) {
      setImages(editorialImages[activePath]);
      return;
    }
    setImages([]);
  }, [activePath, editorialImages]);

  const handleLikeClick = (id: string) => {
    dispatch(setLikeImages(activePath, id));
  };

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchValue(e.target.value);
  };
  const handleSubmit = () => {
    if (searchValue) dispatch(getSearchList(searchValue, activePath));
    else dispatch(getPhotos(activePath));
  };

  const { urls } = images?.[0] || {};
  const mainImageCard = (
    <div className={styles.mainImageWrapper}>
      <img src={urls?.full || ''} className={styles.mainImage} />
      <div className={styles.mainImageTitle}>
        <h1 className={styles.imageTitle}>Unsplash</h1>
        <span className={styles.imageDescription}>The internet’s source for visuals.</span>
        <span className={styles.imageDescription}>Powered by creators everywhere.</span>
      </div>
    </div>
  );

  const imageList = (
    <>
      {images.length !== 0 &&
        images.map((image: any) => {
          const isLikedImage = likedImage?.[activePath]?.includes(image.id) || false;
          return (
            <ImageCard
              url={image.urls.regular}
              customImageClass={styles.listImage}
              alt={image.alt_description}
              imageUserName={image.user.name}
              imageDescription={image.user.bio}
              onLikeClick={() => handleLikeClick(image.id)}
              isImageLike={isLikedImage}
            />
          );
        })}
    </>
  );

  return (
    <div>
      <Header onChange={handleSearch} onClick={handleSubmit} searchValue={searchValue} />
      <Navbar />
      {mainImageCard}
      <div className={styles.imageWrapper}>{imageList}</div>
    </div>
  );
}

export default HomePage;
