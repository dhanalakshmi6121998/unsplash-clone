import { combineReducers } from 'redux';
import reducer from '../HomePage/Action/reducer';

export default combineReducers({
  images: reducer,
});
