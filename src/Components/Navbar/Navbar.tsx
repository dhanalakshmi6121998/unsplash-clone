import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Navbar.module.scss';

function Navbar(props: any) {
  return (
    <div className={styles.navigationMenu}>
      <ul className={styles.linkButton}>
        <li key='homePage'>
          <NavLink
            to='/'
            className={({ isActive }) => {
              return isActive ? styles.active : '';
            }}
          >
            Editorial
          </NavLink>
        </li>
        <div className={styles.divider}></div>
        <li key='wallpapers'>
          <NavLink
            to='/wallpapers'
            className={({ isActive }) => {
              return isActive ? styles.active : '';
            }}
          >
            Wallpapers
          </NavLink>
        </li>
        <li key='travel'>
          <NavLink
            to='/travel'
            className={({ isActive }) => {
              return isActive ? styles.active : '';
            }}
          >
            Travel
          </NavLink>
        </li>
        <li key='nature'>
          <NavLink
            to='/nature'
            className={({ isActive }) => {
              return isActive ? styles.active : '';
            }}
          >
            Nature
          </NavLink>
        </li>
      </ul>
    </div>
  );
}

export default Navbar;
