import React from 'react';
import cn from 'classnames';
import styles from './ImageCard.module.scss';

interface IImageCard {
  url: string;
  alt: string;
  customImageClass: string;
  isImageLike: boolean;
  imageUserName: string;
  imageDescription: string;
  onLikeClick: () => void;
}

function ImageCard(props: IImageCard) {
  const {
    url,
    alt,
    customImageClass,
    isImageLike = false,
    imageUserName = '',
    imageDescription = '',
    onLikeClick,
  } = props || {};
  return (
    <div className={styles.container}>
      <img src={url} alt={alt} className={customImageClass} />

      <div className={styles.overlay}></div>
      <div
        className={cn(
          styles.likeButton,
          isImageLike ? styles.activeLike : styles.primaryLikeButton
        )}
        onClick={onLikeClick}
      >
        <img src='Images/heartIcon.png' alt='Like' />
      </div>
      <div className={styles.imageDetails}>
        <p className={styles.imageUserName}>{imageUserName}</p>
        <p className={styles.imageDescription}>{imageDescription}</p>
      </div>
    </div>
  );
}

export default ImageCard;
