import React from 'react';
import { ISearchInput } from '../../_types/commonTypes';
import SearchInput from '../SearchInput/SearchInput';
import styles from './Header.module.scss';

function Header(props: ISearchInput) {
  const { onChange, onClick, searchValue } = props || {};
  return (
    <div className={styles.headerWrapper}>
      <img src='Images/Logo_of_Unsplash.png' className={styles.logoIcon} alt='logo' />
      <SearchInput onChange={onChange} onClick={onClick} searchValue={searchValue} />
    </div>
  );
}

export default Header;
