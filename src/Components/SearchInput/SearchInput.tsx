import React from 'react';
import { ISearchInput } from '../../_types/commonTypes';
import styles from './SearchInput.module.scss';

function SearchInput(props: ISearchInput) {
  const { onChange, onClick, searchValue } = props || {};

  return (
    <div className={styles.searchWrapper}>
      <div className={styles.search}>
        <button className={styles.searchButton} onClick={onClick}>
          <img src='Images/searchIcon.png' className={styles.searchIcon} />
        </button>
        <input
          type='text'
          className={styles.searchTerm}
          placeholder='Search free high-resolution photos'
          onChange={onChange}
          value={searchValue}
        />
        <span className={styles.searchBarRightIcon}></span>
      </div>
    </div>
  );
}

export default SearchInput;
